const express = require("express");

const app = express();
const mongoose = require("mongoose");
const port = 3000;
require("dotenv").config();


mongoose.connect(process.env.DATABASE_URL,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true 
    });

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.listen(port,()=>console.log(`Server running in ${port}`));

// if a connection occured, output in the console
let db = mongoose.connection
db.on("error", console.error.bind(console,"connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

const userSchema = new mongoose.Schema(
    {
    name: String,
    password:String
    }
)

const User = mongoose.model("User", userSchema);

app.get("/", (req,res)=>
    {
        User.find({},(err, result)=>
            {
                if(err)
                {
                    return console.log(err)
                } else 
                {
                    return res.status(200).json(
                        {
                            data:result
                        }
                    )
                }
            }
        )
    }
)




app.post("/signup",(req,res)=>{
    User.findOne({name:req.body.name},(err,result)=>
        {           
        if(result && result.name == req.body.name)
            {
            return res.send("Username is already been taken");
            }
        else
            {
                let newUser = new User({name:req.body.name,password:req.body.password})
                
                newUser.save((saveErr,savedUser)=>
                    {
                        if(saveErr)
                        {
                            return console.error(saveErr);
                        }else
                        {
                            return res.status(201).send("New user registered");
                        }
                    }
                )
            }
        }    
    )
})

app.use (express.json());
app.use(express.urlencoded({extended:true}))